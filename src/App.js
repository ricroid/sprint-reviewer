import React, { Component } from 'react';
import Cover from './component/Cover';
import Overview from './component/Overview';
import Goal from './component/Goal';
import FrontendBanner from './component/FrontendBanner';
import BackendBanner from './component/BackendBanner';
import './App.css';

const modules = {
  "Module 1":10,
  "Module 2":3,
  "Module 3": 7
};

const frontendGoals = [
  {
    title: "Goal name",
    description: "description",
    problem: "problem",
    solution: "solution",
    stories:
    [
      {
        jira: "4213",
        points: 1,
        module: "Module 1",
        type: "tech-only",
        title: "Move App's componentWillMount lifecycle to componentDidMount"
      },
      {
        jira: "4214",
        points: 1,
        module: "Module 1",
        type: "tech-only",
        title: "Move App's componentWillMount lifecycle to componentDidMount"
      },
      {
        jira: "4215",
        points: 1,
        module: "Module 1",
        type: "tech-only",
        title: "Move App's componentWillMount lifecycle to componentDidMount"
      },
      {
        jira: "4216",
        points: 1,
        module: "Module 1",
        type: "tech-only",
        title: "Move App's componentWillMount lifecycle to componentDidMount"
      }
    ] 
  }
];

const backendGoals = [
  {
    title: "Goal name",
    description: "description",
    problem: "problem",
    solution: "solution",
    stories:
    [
      {
        jira: "4217",
        points: 1,
        module: "Module 1",
        type: "bugfix",
        title: "Move App's componentWillMount lifecycle to componentDidMount"
      },
      {
        jira: "4218",
        points: 1,
        module: "Module 1",
        type: "bugfix",
        title: "Move App's componentWillMount lifecycle to componentDidMount"
      },      
      {
        jira: "4219",
        points: 1,
        module: "Module 1",
        type: "feature",
        title: "Move App's componentWillMount lifecycle to componentDidMount"
      }
    ] 
  }
];

class App extends Component {
  render() {
    return (
      <div className="App">
        <Cover sprint={50}/>
        <Overview modules={modules}/>   
        <FrontendBanner goals={frontendGoals.length} />
        <Goal goals={frontendGoals} />
        <BackendBanner goals={backendGoals.length} />
        <Goal goals={backendGoals}/>
      </div>
    );
  }
}

export default App;

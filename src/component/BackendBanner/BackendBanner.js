import React from "react";
import './BackendBanner.css';

const BackendBanner = ({goals}) => {
  return (
    <div className='backend'>      
      {goals} Goal achieved in backend.
    </div>
  );
}

export default BackendBanner;
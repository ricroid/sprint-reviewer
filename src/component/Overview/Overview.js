import React from "react";
import Graph from "./Graph";
import './Overview.css';

const Overview = props => {
  return (
    <div className='overview'>
      <h1>Overview.</h1>
      <h2>How was the effort distributed this sprint?</h2>
      <Graph modules={props.modules} />
    </div>
  );
}

export default Overview;
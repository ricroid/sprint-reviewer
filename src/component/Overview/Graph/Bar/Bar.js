import React from "react";
import './Bar.css';


const Bar = props => {
  const style = {
    height: props.height+'%'
  };
  return (
    <div className='bar' style={style}>
      <p className='name'>{props.name}</p>
      <p className='value'>{props.value}</p>
    </div>
  );
}

export default Bar;
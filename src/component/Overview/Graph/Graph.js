import React from "react";
import Bar from "./Bar";
import map from "lodash/map";
import max from "lodash/max";
import './Graph.css';


const Graph = ({modules}) => {
  const maxValue = max(Object.values(modules));
  const bars = map(modules, (value, name) => <Bar key={name} name={name} value={value} height={(value*100/maxValue)}/>);

  return (
    <div className='graph'>
      {bars}
    </div>
  );
}

export default Graph;
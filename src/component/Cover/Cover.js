import React from "react";
import './Cover.css';

const Cover = props => {
  return (
    <div className='cover'>
      <h1>Project Name.</h1>
      <h2>Sprint Review {props.sprint}</h2>
    </div>
  );
}

export default Cover;
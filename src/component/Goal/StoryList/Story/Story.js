import React from "react";
import './Story.css';

const Story = props => {

  return (
    <div class={"story "+props.type} storyPoints={props.points}>
      <p class="story__title" jiraId={props.jira}>
        {props.title} 
        <span class="tag module">{props.module}</span>
        <span class="tag type">{props.type}</span>
      </p>
    </div>
   
  );
}

export default Story;
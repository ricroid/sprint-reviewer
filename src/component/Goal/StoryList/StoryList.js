import React from "react";
import Story from "./Story";
import './StoryList.css';

const StoryList = props => {

  const stories = props.stories.map(story => (
    <Story
      key={story.jira} 
      title={story.title}
      points={story.points}
      module={story.module}
      type={story.type}
      jira={story.jira}
    />
  ));

  return (
    <div className='stories'>
      {stories}
    </div>
  );
}

export default StoryList;
import React from "react";
import './GoalDetails.css';

const GoalDetails = props => {
  return (
    <div class="goal">
            <p class="goal__title" goal={props.goalNumber}>{props.title}</p>
            <p class="goal__description">{props.description}</p>
            <div class="goal__details">
                    <div class="goal__details__problem">
                        <svg class="image" viewBox="0 0 24 24">
                            <path d="M9,22A1,1 0 0,1 8,21V18H4A2,2 0 0,1 2,16V4C2,2.89 2.9,2 4,2H20A2,2 0 0,1 22,4V16A2,2 0 0,1 20,18H13.9L10.2,21.71C10,21.9 9.75,22 9.5,22V22H9M10,16V19.08L13.08,16H20V4H4V16H10M13,10H11V6H13V10M13,14H11V12H13V14Z" />
                        </svg>                     
                        <p class="goal__details__problem__description">{props.problem}</p>
                    </div>
                    <div class="goal__details__solution">
                        <svg class="image" viewBox="0 0 24 24">
                            <path d="M20,11H23V13H20V11M1,11H4V13H1V11M13,1V4H11V1H13M4.92,3.5L7.05,5.64L5.63,7.05L3.5,4.93L4.92,3.5M16.95,5.63L19.07,3.5L20.5,4.93L18.37,7.05L16.95,5.63M12,6A6,6 0 0,1 18,12C18,14.22 16.79,16.16 15,17.2V19A1,1 0 0,1 14,20H10A1,1 0 0,1 9,19V17.2C7.21,16.16 6,14.22 6,12A6,6 0 0,1 12,6M14,21V22A1,1 0 0,1 13,23H11A1,1 0 0,1 10,22V21H14M11,18H13V15.87C14.73,15.43 16,13.86 16,12A4,4 0 0,0 12,8A4,4 0 0,0 8,12C8,13.86 9.27,15.43 11,15.87V18Z" />
                        </svg>
                        <p class="goal__details__solution__description">{props.solution}</p>
                    </div>
            </div>
        </div>
  );
}

export default GoalDetails;
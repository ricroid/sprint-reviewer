import React, {Fragment} from "react";
import StoryList from "./StoryList";
import GoalDetails from "./GoalDetails";
import './Goal.css';

const Goal = props => {

    const goals = props.goals.map((goal, index) => (
        <Fragment key={goal.id} >
            <GoalDetails 
                goalNumber={index + 1}
                title={goal.title}
                description={goal.description}
                problem={goal.problem}
                solution={goal.solution}
            />
            <StoryList 
                stories={goal.stories}
            />
        </Fragment>
    ));

  return (
    <div class="content">             
        {goals}
    </div>
  );
}

export default Goal;
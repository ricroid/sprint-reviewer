import React from "react";
import './FrontendBanner.css';

const FrontendBanner = ({goals}) => {
  return (
    <div className='frontend'>      
      {goals} Goal achieved in frontend.
    </div>
  );
}

export default FrontendBanner;